
public class KeckW {

	public static void main(String[] args) {
		//print erzeugt Ausgabe ohne Zeilenumbruch
		System.out.print("Hello World");
		//println erzeugt Ausgabe mit Zeilenumbruch
		System.out.println("Hello World");
		
		/* Dies ist
		 * ein mehrzeiliger
		 * Kommentar */
		
		//arbeiten mit Variablen
		//Deklaration einer ganzzahligen Variable "zahl1"
		//Initialisierung mit dem Wert 5
		int zahl1 = 5;
		int zahl2 = 10;
		int ergebnis;
		String ausgabe = "F�nf plus Zehn ist gleich ";
		ergebnis =  zahl1+zahl2;
		System.out.println(zahl1); //Zahlausgabe
		System.out.println("zahl1"); //Textausgabe
		System.out.println("Die Variable hat den Wert "+zahl1);
		System.out.println("Die Variable hat den Wert "+zahl1+zahl2);
		System.out.println(zahl1+zahl2);
		System.out.println(ergebnis);
		System.out.println(ausgabe + ergebnis);
	}

}
