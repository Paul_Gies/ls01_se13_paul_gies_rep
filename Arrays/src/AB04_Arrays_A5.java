
public class AB04_Arrays_A5 {

	public static void main(String[] args) {
	
		int [] myArray = {23,76,1,68,54,234,889,-9};
		
		
		int max = myArray[0];
		int countmax = 0;
	    for (int i = 0; i < myArray.length; i++) {
	        if (myArray[i] > max) {
	            max = myArray[i];
	            countmax = i;
	        }
		
	}
	    System.out.println(max);
	    System.out.println(countmax+"\n");
	    
	    int min = myArray[0];
		int countmin = 0;
	    for (int i = 0; i < myArray.length; i++) {
	        if (myArray[i] < min) {
	            min = myArray[i];
	            countmin = i;
	        }
		
	}
	    System.out.println(min);
	    System.out.println(countmin);
}
}
