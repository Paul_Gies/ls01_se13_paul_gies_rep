/**
*
* Beschreibung
* Einf�hrung in die Programmierung mit Arrays 
* @version 1.0 vom 25.04.2022
* @Gies, Paul 
*/
import java.util.Scanner;
public class myArray {

public static void main(String[] args) {
  
  Scanner sc = new Scanner (System.in);
  
  //Deklaration eines Arrays, 5 integer Werte
  //Name des Arrays: "intArray"
  
  int [] intArray = new int [5];
  
  //Speichern von Werten im Array
  //Wert 1000 an 3. Stelle des Arrays
  //Wert 500 an Index 4
  
  intArray [2] = 1000;
  intArray [4] = 500;
  
  //Deklaration + Initialisierung eines Arrays in einem Schritt
  //Name des Arrays: "doubleArray"
  // L�nge 3, mit 3 double Werten 
  
  double [] doubleArray = {1.1,2.2,3.3};
  
  //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
  
  System.out.println(doubleArray[1]);
  
  //Nutzer soll einen Index angeben 
  //und an diesen Index einen neuen Wert speichern
  
  System.out.println("An welchen Index (0 bis 4) soll der neue Wert?");
  int index = 0;
  index = sc.nextInt();
 
  System.out.println("Geben Sie den neuen Wert f�r index " +   index   + " an:");
  int zahl = 0;
  zahl = sc.nextInt();
  intArray[index] = zahl;
  
  
  //Alle Werte vom Array intArray sollen ausgegeben werden
  //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
  // 0   0  1000  500  0  (je nachdem welche �nderung vorweg vorgenommen wurde)
  

  for (int i=0; i<intArray.length; i++) {
  System.out.println(intArray[i]);
  }
  
  
  //Der intArray soll mit neuen Werten gef�llt werden
        //1. alle Felder sollen den Wert 0 erhalten
  intArray [0] = 0;
  intArray [1] = 0;
  intArray [2] = 0;
  intArray [3] = 0;
  intArray [4] = 0;
  
  //Der intArray soll mit neuen Werten gef�llt werden
        //2. alle Felder sollen vom Nutzer einen Wert bekommen
        //nutzen Sie dieses Mal die Funktion �length�
  
  for (int i=0; i<intArray.length; i++) {
  System.out.println("Geben Sie den neuen Wert f�r index " +   i   + " an:");
  zahl = 0;
  zahl = sc.nextInt();
  intArray[i] = zahl;
  }
  for (int i=0; i<intArray.length; i++) {
	  System.out.println(intArray[i]);
	  }
  System.out.println();
  //Der intArray soll mit neuen Werten gef�llt werden
         //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
  
  for (int i=0; i<intArray.length; i++) {
	  zahl = 0;
	  zahl = 10 + i*10;
	  intArray[i] = zahl;
	  }
  for (int i=0; i<intArray.length; i++) {
	  System.out.println(intArray[i]);
	  }
  System.out.println("\nEnd of Program");
} // end of main

} // end of class arrayEinfuehrung
