import java.util.Scanner;

public class AB04_Arrays_A1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

//Aufgabe 1
	//1. Erzeugen Sie ein Array namens myArray, das 10 Werte speichern kann.
		int [] myArray = new int [10];
	//2. Speichern Sie 10 beliebige Werte in Ihrem Array.
		for (int i=0; i<myArray.length; i++) {
			  int zahl = 0;
			  zahl = i*10;
			  myArray[i] = zahl;
			  }
		  
	//3. Lassen Sie mit Hilfe einer for-Schleife die 10 Werte ausgeben.
		for (int i=0; i<myArray.length; i++) {
			  System.out.println(myArray[i]);
			  }
	//4. Speichern Sie am Index 3 den Wert 1000.
			myArray[3] = 1000;
	//5. Lassen Sie zum �berpr�fen den Wert des Index 3 ausgeben.
			System.out.println(myArray[3]);
	//6. �berschrieben Sie alle Werte mit Hilfe einer for-Schleife, so dass nur noch 0-en in Ihrem Array stehen.
			for (int i=0; i<myArray.length; i++) {
				  int zahl = 0;
				  myArray[i] = zahl;
				  }
	//7. Erstellen Sie nun ein Men� mit folgenden Punkten:
	//a. Alle Werte ausgeben
	//b. Einen bestimmten Wert ausgeben
	//c. Das Array komplett mit neuen Werten bef�llen
	//d. Einen bestimmten Wert �ndern
			int schleife = 1;
			do {
			System.out.println("Bitte w�hlen Sie aus!");
			System.out.println("1. Alle Werte ausgeben");
			System.out.println("2. Einen bestimmten Wert ausgeben");
			System.out.println("3. Das Array komplett mit neuen Werten bef�llen");
			System.out.println("4. Einen bestimmten Wert �ndern");
			int auswahl = sc.nextInt();
			int auswahlIndex = 0;
			
			
			switch (auswahl) {
			case 1:
				for (int i=0; i<myArray.length; i++) {
					  System.out.println(myArray[i]);
					  }
				break;
			
			case 2:
				System.out.println("Welchen Wert wollen Sie ausgeben (0 -10)?");
				auswahlIndex = sc.nextInt();
				System.out.println("Der Wert f�r Index " + auswahlIndex + " ist " + myArray[auswahlIndex] + ".");
				break;
				
			case 3:
				for (int i=0; i<myArray.length; i++) {
					  System.out.println("Geben Sie den neuen Wert f�r index " +   i   + " an:");
					  int zahl = 0;
					  zahl = sc.nextInt();
					  myArray[i] = zahl;
					  }
				break;
				
			case 4:
				System.out.println("Welchen Wert wollen Sie �ndern (0 -10)?");
				auswahlIndex = sc.nextInt();
				System.out.println("Welchen Wert wollen Sie f�r Index " + auswahlIndex + " neu definieren?");
				int zahlNeu = sc.nextInt();
				myArray[auswahlIndex] = zahlNeu;
				System.out.println("Der Index " + auswahlIndex + " wurde mit " + zahlNeu + " neu definiert!");
				break;
			}
			
			System.out.println("M�chten Sie das Programm wiederholen?");
			System.out.println("1. Ja, bitte!");
			System.out.println("2. Nein, danke!");
			int schleifeAuswahl = sc.nextInt();
			schleife = schleifeAuswahl;
			
	//8. Erweitern Sie Ihr Programm von 7. so, dass der Benutzer entscheiden kann, ob es wiederholt werden soll oder nicht. 

			}while(schleife == 1);
		sc.close();
		
	}

}


