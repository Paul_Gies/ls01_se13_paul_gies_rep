
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Konsolenaufgabe (�bung 2)
		System.out.println("Konsolenaufgabe (�bung 2):\n");
		
		
		//Aufgabe 1:
		System.out.println("Aufgabe 1:\n");
		
		System.out.printf("%5s\n","**");
		System.out.printf("*");
		System.out.printf("%7s\n","*");
		System.out.printf("*");
		System.out.printf("%7s\n","*");
		System.out.printf("%5s\n","**");
		
		
		//Aufgabe 2:
		System.out.println("\nAufgabe 2:\n");
		
		System.out.printf("%-5s","0!");
		System.out.printf("%-19s","=");
		System.out.print(" =");
		System.out.printf("%4s","1");
				
		System.out.printf("\n%-5s","1!");
		System.out.printf("%-19s","= 1");
		System.out.print(" =");
		System.out.printf("%4s","1");
				
		System.out.printf("\n%-5s","2!");
		System.out.printf("%-19s","= 1 * 2");
		System.out.print(" =");
		System.out.printf("%4s","2");
		
		System.out.printf("\n%-5s","3!");
		System.out.printf("%-19s","= 1 * 2 * 3");
		System.out.print(" =");
		System.out.printf("%4s","6");
		
		System.out.printf("\n%-5s","4!");
		System.out.printf("%-19s","= 1 * 2 * 3 * 4");
		System.out.print(" =");
		System.out.printf("%4s","24");
		
		System.out.printf("\n%-5s","5!");
		System.out.printf("%-19s","= 1 * 2 * 3 * 4 * 5");
		System.out.print(" =");
		System.out.printf("%4s","120");
	
		
		//Aufgabe 3:
		System.out.println("\n");
		System.out.println("\nAufgabe 3:\n"
				+ "");
		
		System.out.printf("%-12s| %10s\n", "Fahrenheit" , "Celsius");
		System.out.println("-------------------------");
		System.out.printf("%-12d| %10.2f\n", -20 , -28.8889);
		System.out.printf("%-12d| %10.2f\n", -10 , -23.3333);
		System.out.printf("%+-12d| %10.2f\n", 0 , -17.7778);
		System.out.printf("%+-12d| %10.2f\n", 20 , -6.6667);
		System.out.printf("%+-12d| %10.2f\n", 30 , -1.1111);
		
		
		
	}
	
}
