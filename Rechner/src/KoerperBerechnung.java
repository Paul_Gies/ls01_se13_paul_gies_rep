import java.util.Scanner;

public class KoerperBerechnung {
	
	public static void main(String[] args) { //Start  of Main
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Willkommen beim Volumenrechner!");
		System.out.println("Bitte w�hlen Sie den zu berechnenden K�rper!");
		
		byte startStop = 1;
		while (startStop > 0) {
		
		String koerper;
		double erg;
		
		System.out.printf("\n%18s", "1 - W�rfel");
		System.out.printf("\n%18s", "2 - Quader");
		System.out.printf("\n%20s", "3 - Pyramide");
		System.out.printf("\n%18s", "4 - Kugel\n");
		System.out.print("\nIhre Auswahl: ");
		koerper = tastatur.next();
		
		switch (koerper) {		//Start of Switch	
		case "1": //W�rfel
			System.out.println("Sie haben sich f�r die W�rfelvolumenberechnung entschieden!");
			System.out.println("Die Formel lautet: V = a * a * a");
			System.out.println("Bitte Geben Sie einen Wert f�r a (in cm) ein!");
			System.out.print("a = ");
			double w_a = tastatur.nextDouble();
			erg = wuerfel(w_a);
			System.out.printf("\n%s%.2f%s%.2f%s\n", "Das Volumen eines W�rfels mit a = ", w_a , "cm betr�gt: " , erg , "cm�.\n");
			break;
			
		case "2": //Quader
			System.out.println("Sie haben sich f�r die Quadervolumenberechnung entschieden!");
			System.out.println("Die Formel lautet: V = a * b * c");
			System.out.println("Bitte Geben Sie einen Wert f�r a (in cm) ein!");
			System.out.print("a = ");
			double q_a = tastatur.nextDouble();
			System.out.println("Bitte Geben Sie einen Wert f�r b (in cm) ein!");
			System.out.print("b = ");
			double q_b = tastatur.nextDouble();
			System.out.println("Bitte Geben Sie einen Wert f�r c (in cm) ein!");
			System.out.print("c = ");
			double q_c = tastatur.nextDouble();
			erg = quader (q_a,q_b,q_c);
			System.out.printf("\n%s%.2f%s%.2f%s%.2f%s%.2f%s\n", "Das Volumen eines Quaders mit a = ", q_a , "cm, b = ", q_b, "cm, c = ", q_c , "cm betr�gt: " , erg , "cm�.\n");
			break;
		
		case "3": //Pyramide
			System.out.println("Sie haben sich f�r die Pyramidenvolumenberechnung entschieden!");
			System.out.println("Die Formel lautet: V = a * a * h / 3");
			System.out.println("Bitte Geben Sie einen Wert f�r a (in cm) ein!");
			double p_a = tastatur.nextDouble();
			System.out.println("Bitte Geben Sie einen Wert f�r h (in cm) ein!");
			double p_h = tastatur.nextDouble();
			erg = pyramide (p_a, p_h);
			System.out.printf("\n%s%.2f%s%.2f%s%.2f%s\n", "Das Volumen einer Pyramide mit a = ", p_a , "cm, b = ", p_h ,  "cm betr�gt: " , erg , "cm�.\n");
			break;
		
		case "4": //Kugel
			System.out.println("Sie haben sich f�r die Kugelvolumenberechnung entschieden!");
			System.out.println("Die Formel lautet: V = 4/3 * r� * Pi");
			System.out.println("Bitte Geben Sie einen Wert f�r r (in cm) ein!");
			double k_r = tastatur.nextDouble();
			erg = kugel (k_r);
			System.out.printf("\n%s%.2f%s%.2f%s\n", "Das Volumen einer Kugel mit r = ", k_r , "cm betr�gt: " , erg , "cm�.\n");
			break;
			
		} //End of Switch
		
		System.out.println("\nDanke das Sie unser Programm nutzen!");
		System.out.println("Bitte bewerten Sie uns auf Google!\n");
		System.out.println("---------------------------------------------------------------------\n");
		
		System.out.println("Willkommen zur�ck!");
		System.out.println("Bitte w�hlen Sie erneut einen K�rper zur Volumenberechnung aus!");
		
		} // End of while
		
		tastatur.close();
		
} // End of Main
		
		public static double wuerfel (double a) {
			return a*3;
	} // End of wuerfel
		
		public static double quader (double a, double b, double c) {
			return a*b*c;
	} // Ende of quader
		
		public static double pyramide (double a, double h) {
			return ((a*a*h)/3);
	} //End of pyramide
		
		public static double kugel (double r) {
			return (4/3)*(r*r*r)*3.14159265359;
	} //End of kugel
		
} //End of class
		
		
	


