
public class Konsolenausgabe {

	private static final int Java = 0;

	public static void main(String[] args) {
		
		System.out.println("Hello World\n");
		System.out.println("Hello World Hola Mundo\n");
		
		int alter = 24;
		String name = "Paul";
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahre alt!\n");
		
		System.out.printf("Hello %s!?!?!??!\n", "World");
		
		String s = "Java-Programm";
		
				System.out.printf( "\n|%s|\n", s );		
				System.out.printf( "\n|%20s|\n", s );		
				System.out.printf( "\n|%-30s|\n", s );		
				System.out.printf( "|%5s|\n", s );
	
				int i = 123;
				
				System.out.printf( "|%d| |%d|\n" , i, -i);
				
				System.out.printf( "|%5d| |%5d|\n" , i, -i); 
				System.out.printf( "|%d| |%d10|\n" , i, -i);
				
				System.out.printf( "|%5d| |%200d|\n" , i, -i); 
				
				
				System.out.println("\nBREAK!\n");
				
				/*
				 * Aufgabenblatt 1: Konsolenausgabe �bung 1
				 * 
				 * @Version1.0 vom 28.09.2021
				 * @Gies, Paul
				 */
				
				System.out.println("�bung 1\n");
				
				//Aufgabe 1:
				System.out.println("Aufgabe 1:\n");
				
				System.out.print("Heute ist noch nicht Wochenende. ");
				System.out.print("Morgen ist auch noch nicht Wochenende.");
				System.out.println(" Und viel schlimmer ist...Nichtmal �bermorgen ist Wochenende!\n");

				System.out.println("�ber�bermorgen ist auch noch nicht Wochenende! ");
				System.out.println("Warum ist das Wochenende noch so fern?");
				
				//Aufgabe 2:
				System.out.println("\nAufgabe 2:");
				
				System.out.printf("\n%20s", "*");
				System.out.printf("\n%21s", "***");
				System.out.printf("\n%22s","*****");
				System.out.printf("\n%23s","*******");
				System.out.printf("\n%24s","*********");
				System.out.printf("\n%25s","***********");
				System.out.printf("\n%26s","*************");
				System.out.printf("\n%21s","***");
				System.out.printf("\n%22s", "***\n");
				
				//Aufgabe 3:
				System.out.println("\nAufgabe 3:\n");
				
				double a = 22.42434243;
				double b = 111.222;
				double c = 4.0;
				double d = 1000000.551;
				double e = 97.34;
				
				
				System.out.printf("1. %.2f\n",		a);
				System.out.printf("2. %.2f\n",		b);
				System.out.printf("3. %.2f\n",		c);
				System.out.printf("4. %.2f\n",		d);
				System.out.printf("5. %.2f\n",		e);
				
				
	}
}
