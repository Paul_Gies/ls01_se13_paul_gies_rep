
public class WeltDerZahlen {

	public static void main(String[] args) {
		//https://moodle.oszimt.de/mod/resource/view.php?id=221507
		
		// Die Anzahl der Planeten in unserem Sonnesystem 
		byte anzahlPlaneten = 8;
		
		// Anzahl der Sterne in unserer Milchstra�e
		long anzahlSterne = 150000000;
		
		//Wie viele Einwohner hat Berlin?
		int bewohnerBerlin = 3700000;
		
		// Wie alt bist du?  Wie viele Tage sind das?
		byte alterJahre = 24;
		short alterTage = 8760;
		
		// Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 150000;
		
		// Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		int flaecheGroesstesLand = 17098242;

		// Wie gro� ist das kleinste Land der Erde?
	    double flaecheKleinsteLand = 0.44;
	    
	    System.out.println("Die Anzahl der Planeten in unserem Sonnensystem ist " + anzahlPlaneten + ".");
	    System.out.println("Die Anzahl der Sterne in unserer Milchstrasse ist " + anzahlSterne + ".");
	    System.out.println("Die Anzahl der Einwohner in Berlin ist " + bewohnerBerlin + ".");
	    System.out.println("Ich bin " + alterJahre + " Jahre alt. Das sind in Tagen: " + alterTage + " Tage.");
	    System.out.println("Das schwerste Tier der Welt ist der Blauwal und wiegt bis zu " + gewichtKilogramm + "kg.");
	    System.out.println("Das groesste Land der Welt ist Russland und hat eine Flaeche von " + flaecheGroesstesLand +"km�.");
	    System.out.println("Das kleinste Land der Welt ist die Vatikanstadt und hat eine Flaeche von " + flaecheKleinsteLand +"km�.");
	}

}
