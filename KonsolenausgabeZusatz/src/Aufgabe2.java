import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		
	/*Ein Investor erh�lt f�r eine bestimmte Einzahlung j�hrlich eine Zinsenzahlung. 
	Wie viel Euro wurden nach n Jahren ausgezahlt, wenn die Zinsen jeweils am 
	Jahresende ausgezahlt werden? */
		
		System.out.println("Guten Tag!");
		System.out.print("Bitte zahlen Sie den gew�nschten Betrag ein: ");
		
		Scanner tastatur = new Scanner(System.in);
		
		double einzahlung = tastatur.nextDouble();
		double zinsenInProzent = 3;
		double summe;
		
		System.out.printf("%s%.2f%s", "Ihre Einzahlung betr�gt: " , einzahlung , "�\n");
		
		//potentiell Platz f�r y/n switch
		
		System.out.printf("%s%.2f%s", "Der Jahreszins bei der Bank ihres Vertrauens betr�gt: " , zinsenInProzent , "%\n");
		
		System.out.println("\nF�r wie viele Jahre wollen Sie ihre Einzahlung deponieren?");
		System.out.print("Bitte geben Sie ihre Pr�ferenz in ganzen Jahren an: ");
		
		double jahre = tastatur.nextDouble();
		System.out.printf("%s%.2f%s%.0f%s", "Wir werden Ihre Einzahlung in H�he von " , einzahlung , "� f�r " , jahre , " Jahre bei uns hinterlegen.\n");
		
		summe = einzahlung + (((einzahlung/100)*zinsenInProzent)*jahre);
		System.out.printf("\n%s%.0f%s%.2f%s", "Sie werden in " , jahre , " Jahren insgesamt " , summe , "� erhalten!");
		
		
	}

}
