//@Gies,Paul | 28.02.2022
import java.util.Scanner;

public class ifelse {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a=0,b=0;
		double c=0,d=0,e=0,f=0,g=0;
		boolean loop1=true;
		
		/*1. Aufgabe 
		Der Benutzer gibt 2 ganzzahlige Werte ein. Wenn die Eingaben gleich sind, dann Ausgabe 
		"Gleiche Werte". */ 
		System.out.println("Aufgabe 1");
		System.out.print("Bitte geben Sie einen Wert f�r a ein: ");
		a = sc.nextInt();
		System.out.print("Bitte geben Sie einen Wert f�r b ein: ");
		b = sc.nextInt();
		if(a == b)
		{
			System.out.println("Gleiche Werte");
		}
		else
		{
			System.out.println("Ungleiche Werte");
		}
		
		/*2. Aufgabe 
		Der Benutzer gibt zwei reelle Zahlen ein. Falls die Zahlen gleich gro� sind, werden sie 
		nebeneinander ausgegeben, sonst untereinander, die kleinere zuerst. */
		System.out.println("Aufgabe 2");
		System.out.print("Bitte geben Sie einen Wert f�r c ein: ");
		c = sc.nextDouble();
		System.out.print("Bitte geben Sie einen Wert f�r d ein: ");
		d = sc.nextDouble();
		if(c == d)
		{
			System.out.println(c+" "+d);
		}
		else if(c > d)
		{
			System.out.println(d+"\n"+c);
		}
		else if(c < d)
		{
			System.out.println(c+"\n"+d);
		}
		
		/*3. Aufgabe 
		Der Benutzer gibt eine Zahl ein, es soll gepr�ft werden, ob sie negativ oder positiv ist. Das 
		Ergebnis wird ausgegeben. */
		System.out.println("Aufgabe 3");
		System.out.print("Bitte geben Sie einen Wert f�r e ein: ");
		e = sc.nextDouble();
		if(e>0)
		{
			System.out.println("Die Zahl "+e+" ist positiv.");
		}
		else if(e<0)
		{
			System.out.println("Die Zahl "+e+" ist negativ.");
		}
		
		
		/*4. Aufgabe   
		Der Benutzer soll eine Zahl zwischen 0 und 100 eingeben (einschlie�lich der Grenzen). Eine 
		Zahl au�erhalb des Bereichs wird mit "Fehlerhafte Eingabe" quittiert.  
		Eine Zahl >50 mit "gro�", sonst "klein".*/
		System.out.println("Aufgabe 4");
		while(loop1==true)
		{
			System.out.print("Bitte geben Sie einen Wert (0 bis 100) f�r f ein: ");
			f = sc.nextDouble();
			if(f>=0 && f<=50)
			{
				System.out.println("klein");
				break;
			}
			else if(f>=51 && f<=100)
			{
				System.out.println("gro�");
				break;
			}
			else if(f<0 || f>100)
			{
				System.out.println("Fehlerhafte Eingabe");
			}
		}
		
		/*5. Aufgabe  
		Der Benutzer gibt eine ganze Zahl ein. Falls die Zahl gerade ist, soll sie durch 2 geteilt werden, 
		sonst soll "Zahl ungerade" ausgegeben werden.  
		Verwenden Sie f�r die Aufgabe den Modulooperator %.*/
		System.out.println("Aufgabe 5");
		while(loop1==true)
		{
		System.out.print("Bitte geben Sie einen Wert f�r g ein: ");
		g = sc.nextDouble();
			if(g%2==0)
			{
				g = g/2;
				System.out.println("Zahl ist gerade und geteilt durch 2: "+g);
				break;
			}
			else if(g%2!=0)
			{
				System.out.println("Zahl ungerade");
			}
		}
		sc.close();
	}


	}


