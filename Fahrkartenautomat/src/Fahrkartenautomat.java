﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args)
    {
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       boolean loop = true;
      
       
       while(loop = true) {
       //Fahrkartenbestellung
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
          
       // Geldeinwurf
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
             
       //Fahrscheinausgabe
       fahrkartenAusgeben();
              
       // Rückgeldberechnung und -Ausgabe
       rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
       }
    }//End of Main
public static double fahrkartenbestellungErfassen() {
    //fahrkartenbestellungErfassen
    double zuZahlenderBetrag = 0; 
	   char euro ='€';
	   int auswahl;
	   double [] preis = new double [10];
	   String [] bezeichnung = new String [10];
 
	bezeichnung [0] = "Einzelfahrschein Berlin AB"; 
	preis [0] = 2.90;
	bezeichnung [1] = "Einzelfahrschein Berlin BC";
	preis [1] = 3.30;
	bezeichnung [2] = "Einzelfahrschein Berlin ABC";
	preis [2] = 3.60;
	bezeichnung [3] = "Kurzstrecke";
	preis [3] = 1.90;
	bezeichnung [4] = "Tageskarte Berlin AB";
	preis [4] = 8.60;
	bezeichnung [5] = "Tageskarte Berlin BC";
	preis [5] = 9.00;
	bezeichnung [6] = "Tageskarte Berlin ABC";
	preis [6] = 9.60;
	bezeichnung [7] = "Kleingruppen-Tageskarte Berlin AB";
	preis [7] = 23.50;
	bezeichnung [8] = "Kleingruppen-Tageskarte Berlin BC";
	preis [8] = 24.30;
	bezeichnung [9] = "Kleingruppen-Tageskarte Berlin ABC";
	preis [9] = 24.90;
 
 boolean loop = true;
 int anzahl;
	   
 while (loop == true) {
	   System.out.println("\nFahrkartenbestellvorgang:\n" + "=====================================================");
	   System.out.println("\nWählen Sie ihre Wunschfahrkarte für den Tarifbereich Berlin aus:");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[0], preis[0] , "(1)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[1], preis[1] , "(2)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[2], preis[2] , "(3)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[3], preis[3] , "(4)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[4], preis[4] , "(5)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[5], preis[5] , "(6)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[6], preis[6] , "(7)\n");
	   System.out.printf("%-34s [%.2f]%5s",bezeichnung[7], preis[7] , "(8)\n");
	   System.out.printf("%-34s [%.2f]%5s",bezeichnung[8], preis[8] , "(9)\n");
	   System.out.printf("%-34s [%.2f]%6s",bezeichnung[9], preis[9] , "(10)\n");
	   System.out.printf("%s%40s","Bezahlen", "(11)\n");
	   System.out.println("\n=====================================================");
	  
	  
	   System.out.print("Ihre Wahl: ");
	   auswahl = tastatur.nextInt();
	   
 switch (auswahl) { //Start of Switch	
		case 1: //Einzelfahrschein Berlin AB [2,90 EUR] (Index0)
			anzahl = Ticketzahl();
			zuZahlenderBetrag =  zuZahlenderBetrag + preis[0] * anzahl;
		  	System.out.println("Ihre Wahl: "+ bezeichnung[0]);
		  	System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 2: //Einzelfahrschein Berlin BC [3,30 EUR] (Index1)
			anzahl = Ticketzahl();
			zuZahlenderBetrag =  zuZahlenderBetrag + preis[1] * anzahl;
		  	System.out.println("Ihre Wahl: "+ bezeichnung[1]);
		  	System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 3: //Einzelfahrschein Berlin ABC [3,60 EUR] (Index2)
			anzahl = Ticketzahl();
			zuZahlenderBetrag =  zuZahlenderBetrag + preis[2] * anzahl;
		  	System.out.println("Ihre Wahl: "+ bezeichnung[2]);
		  	System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 4: //Kurzstrecke [1,90 EUR] (Index3)
			anzahl = Ticketzahl();
			zuZahlenderBetrag =  zuZahlenderBetrag + preis[3] * anzahl;
		  	System.out.println("Ihre Wahl: "+ bezeichnung[3]);
		  	System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 5: // Tageskarte Berlin AB [8,60 EUR] (Index4)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag = zuZahlenderBetrag + preis[4] * anzahl;
			System.out.println("Ihre Wahl: " + bezeichnung[4]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 6: // Tageskarte Berlin ABC [9,00 EUR] (Index5)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag = zuZahlenderBetrag + preis[5] * anzahl;
			System.out.println("Ihre Wahl: "+ bezeichnung[5]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 7: // Tageskarte Berlin ABC [9,60 EUR] (Index6)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag = zuZahlenderBetrag + preis[6] * anzahl;
			System.out.println("Ihre Wahl: "+ bezeichnung[6]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
		
		case 8: //Kleingruppen-Tageskarte Berlin AB [23,50 EUR] (Index7)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag =  zuZahlenderBetrag + preis[7] * anzahl;
			System.out.println("Ihre Wahl: "+ bezeichnung[7]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 9: //Kleingruppen-Tageskarte Berlin AB [24,30 EUR] (Index8)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag =  zuZahlenderBetrag + preis[8] * anzahl;
			System.out.println("Ihre Wahl: "+ bezeichnung[8]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 10: //Kleingruppen-Tageskarte Berlin AB [24,30 EUR] (Index9)
			anzahl = Ticketzahl();
			 zuZahlenderBetrag =  zuZahlenderBetrag + preis[9] * anzahl;
			System.out.println("Ihre Wahl: "+ bezeichnung[9]);
			System.out.printf("%s%.2f€","Zwischensumme: " , zuZahlenderBetrag);
			break;
			
		case 11: //Bezahlen (11)
			System.out.println("Bezahlvorgang wird eingeleitet...");
			System.out.printf("%s%.2f€\n","Zwischensumme: " , zuZahlenderBetrag);
			loop = false;
			break;	
			
		default: 
			System.out.println("\n>>Bitte geben Sie eine valide Zahl ein!");
			
		} //End of Switch
	   
}//End of while
	   
 System.out.print("Zu zahlender Betrag (in " + euro + "): ");
 
 return zuZahlenderBetrag;
}//fahrkartenBezahlen

public static double fahrkartenBezahlen (double zuZahlenderBetrag){
	 
    double eingezahlterGesamtbetrag = 0;
    double eingeworfeneMünze=0;
    char euro ='€';

    
    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    {
 	   System.out.printf("%s %.2f", "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
 	   System.out.println(" " + euro);
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2" + euro + "" + " ): ");
 	   
 	   eingeworfeneMünze = tastatur.nextDouble();
 	  //eingeworfeneMünze = 0.5;
 	   
       eingezahlterGesamtbetrag += eingeworfeneMünze;
    }	
	return eingezahlterGesamtbetrag;
   
}//End of fahrkartenBezahlen

	//fahrkartenAusgeben
public static void fahrkartenAusgeben() {
	System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 18; i++)
    {
       System.out.print("=>");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

    System.out.print("");
    }
}//End of fahrkartenAusgeben

	//rueckgeldAusgeben
public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
	
	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	char euro = '€';
	
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("\n%s %.2f", "Der Rückgabebetrag in Höhe von" , rückgabebetrag );
 	   System.out.print(" " + euro);
 	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 " + euro);
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 " + euro);
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 ct");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 ct");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 ct");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 ct");
	          rückgabebetrag -= 0.05;
        } 
        
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.\n");
 }//End of rueckgeldAusgeben

public static int Ticketzahl () {
	boolean loop1 = true;
	int anzahl1 = 0;
	while(loop1 == true) {
		System.out.println("Wie viele Tickets möchten Sie?");
		anzahl1 = tastatur.nextInt();
		System.out.println(anzahl1);
		if (anzahl1 >= 1 && anzahl1 <= 10) {
			break;
			}//End of if
		else {
			System.out.println(">> Bitte wählen Sie eine Anzahl von 1 bis 10 Tickets aus.");
		}//End of else
	} // End of Loop
	return anzahl1;
}	

}//End of class
