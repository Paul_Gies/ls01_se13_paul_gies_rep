
public class VariablenVerwenden {

	 public static void main(String [] args){
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
		          Vereinbaren Sie eine geeignete Variable */
		 
		 int anzahlProgrammdurchläufe;	
		 
		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
		          und geben Sie ihn auf dem Bildschirm aus.*/
		 
		 anzahlProgrammdurchläufe = 25;
		 System.out.println("Die Anzahl an Programmdurchläufen beträgt " + anzahlProgrammdurchläufe + ".");

		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
		          eines Programms ausgewaehlt werden.
		          Vereinbaren Sie eine geeignete Variable */

		 char menue;
		 menue = '?';
		 
		    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
		          und geben Sie ihn auf dem Bildschirm aus.*/

		 menue = 'C';
		 System.out.println("Der Menuepunkt hat den Buchstaben " + menue + ".");
		 
		    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		          notwendig.
		          Vereinbaren Sie eine geeignete Variable */

		 long licht;
		 
		    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
		          und geben Sie sie auf dem Bildschirm aus.*/

		 licht = 299792458;
		 System.out.println("Die Lichtgeschwindigkeit beträgt " + licht + "m/s.");
		 
		    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
		          soll die Anzahl der Mitglieder erfasst werden.
		          Vereinbaren Sie eine geeignete Variable und initialisieren sie
		          diese sinnvoll.*/

		 byte vereinMitglieder = 7;
		 
		    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

		 System.out.println("Der Verein hat " + vereinMitglieder + " Mitglieder.");
		 
		    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
		          dem Bildschirm aus.*/

		 double elekElementarladung;
		 elekElementarladung = 0.0000000000000000001602;
		 System.out.println("Die elektrische Elementarladung beträgt " + elekElementarladung + "As.");
		 
		    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		          Vereinbaren Sie eine geeignete Variable. */

		 boolean buchhaltungTrue, buchhaltungFalse;
		 
		    /*11. Die Zahlung ist erfolgt.
		          Weisen Sie der Variable den entsprechenden Wert zu
		          und geben Sie die Variable auf dem Bildschirm aus.*/
		 
		 buchhaltungTrue = true;
		 buchhaltungFalse = false;
		 System.out.println("Wurde das Buch: (Die Firma, von John Grisham) zurückgegeben? Abfrage: " + buchhaltungTrue);
		 
		 int _zaehler = 1;
	 }
}